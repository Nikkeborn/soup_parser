import requests
from bs4 import BeautifulSoup
# import re
import csv
import os

def get_html(url_, params_=None):
    r = requests.get(url_, params=params_)
    print(r.url)
    print(r.status_code)
    if r.status_code == requests.codes.ok:
        # print(r.text)
        html = r.text
        return html
    else:
        print(f'server error code: {r.status_code}')


def get_ip_port(html):
    soup = BeautifulSoup(html, 'lxml')
    # print(soup)
    # print(soup.prettify())
    soup_ip_port = soup.find('table', id='proxylisttable').find_all('td', class_='')
    # print(type(soup_ip_port))
    # print(len(soup_ip_port))
    # print(soup_ip_port)    
    # soup_ip = soup.find('table', id='proxylisttable').find_all(string=re.compile(r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b'))
    # print(type(soup_ip))
    # print(len(soup_ip))
    # # print(soup_ip)
    # soup_port = soup.find('table', id='proxylisttable').find_all(string=re.compile(r'\b\d{1,5}\b'))#????????????
    # print(type(soup_port))
    # print(len(soup_port))
    # # print(soup_port)
    # soup_test = soup.find('table', id='proxylisttable').find_all(string=re.compile(r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\d{1,5}\b'))#??????????????????
    # print(type(soup_test))
    # print(len(soup_test))
    # # print(soup_test)
    ######################################
    return soup_ip_port
    # return soup_ip
    # return soup_port
    # return soup_test
   

# ip_pattern = r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b'
# port_pattern = r'\b\d{1,5}\b'
# ip_port_pattern = r'\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}()\d{1,5}\b'#??????????????????????

# def re_compile(pattern):
#     c_pattern = re.compile(pattern)
#     print(c_pattern)
#     return c_pattern

# ip_regex = re_compile(ip_pattern)
# print(ip_regex)
# port_regex = re_compile(port_pattern)
# print(port_regex)
 

def to_list(soup_list) -> list:
    all_list = []
    for td in soup_list:
        # print(type(td), td)
        # all_list.append(td)
        # in_td = td.attrs
        # in_td = td.name
        # in_td = td.string
        # in_td = td.children
        # in_td = td.descendants
        # in_td = td.stripped_strings
        # in_td = td.next_sibling 
        # in_td = td.previous_sibling
        # in_td = td.contents# list of lists
        # in_td = td.get_text()# list of strings
        in_td = td.text# list of strings
        # print(type(in_td), in_td)
        all_list.append(in_td)
    # print(len(all_list))    
    # print(all_list)
    return all_list


def strip_list(all_list_):
    strip_list = []
    for it in all_list_:
        it = it.split(' ')
        it = ''.join(it)
        # print(it)
        if it.isalpha():
            pass
        else:
            strip_list.append(it)
    # print(strip_list)
    # print(len(strip_list))
    return strip_list

    
def WrapperToFile():
            def wrpp(fn):
                def arg(list_ok):
                    """
                    Write data to a CSV file path
                    """
                    file_name: str = str(fn.__name__)+'.csv'
                    dir_name = os.getcwd()
                    file_path = os.path.join(dir_name, file_name)
                    print(f'check out file on path: {file_path}')
                    with open(file_path, "w", newline='\r\n') as csv_file:
                        writer = csv.writer(csv_file, delimiter=',')
                        
                        header = ['N','IP:Port']
                        writer.writerow(header)
                        
                        for num, text in enumerate(fn(list_ok)):
                            writer.writerow((num + 1, text))

                    return fn(list_ok)

                return arg            
                
            return wrpp


@WrapperToFile()
def result_list(strip_list_):
    rl = []
    i = 0
    while i != len(strip_list_):
        rl.append(strip_list_[i]+':'+strip_list_[i+1])
        i += 2        
    # print(rl)
    # print(len(rl))
    return rl


def main(link_, keys_=None):
    html = get_html(link_, params_=keys_)
    # print(html)
    soup_ip_port = get_ip_port(html)
    all_list = to_list(soup_ip_port)
    # print(all_list)
    str_li = strip_list(all_list)
    # print(str_li)
    results = result_list(str_li)
    print(results)
    print(len(results))


if __name__ == '__main__':
    URL = 'https://www.us-proxy.org'
    payload = {'key1': None, 'key2': None} 
    
    main(URL, keys_=payload)
